# define a calculator class that other functions can call

class Calculator:
    def __init__(self):
        self.value = 0

    def add(self, num):
        self.value += num

    def subtract(self, num):
        self.value -= num

    def multiply(self, num):
        self.value *= num

    def divide(self, num):
        self.value /= num

    def square(self, num):
        self.value **= num

    def squareroot(self, num):
        self.value **= 0.5

    def cube(self, num):
        self.value **= 3


    

